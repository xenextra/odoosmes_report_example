# -*- coding: utf-8 -*-
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
{
    "name": "Excel, Word, Pdf Report Example",
    'category': 'Extra Tools',
    "summary": """Example Report ODS, ODT convert to docx, pdf, xlsx""",
    "version": "9.0",
    "author": "Cybers Thang",
    "support": "ducthangict.dhtn@gmail.com",
    "license": "AGPL-3",
    "website": "https://odoosmes.com",
    "depends": [
        "odoosmes_report_py3o_convert",
    ],
    'data': [
        'report/SO/docx/sale_order.xml'
    ],
    'images': ['static/description/bg.png'],
    "installable": True,
    "application": True,
}
