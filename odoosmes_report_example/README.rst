  .. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

===========================
Convert Py3o Report to XLSX, XLS, DOCX, DOC, PDF ...
===========================
Document Using and Install at here `https://odoosmes.com <https://odoosmes.com/huong-dan-cai-dat-va-su-dung-report-py3o-va-odoosmes-convert-report>`_ .
This module allows to convert ods, odt report to xlsx, xls, pdf, docx ... with libreoffice.

Bug Tracker
===========

Contributors
------------

* Cybers Thang <ducthangict.dhtn@gmail.com> 


Maintainer
----------

This module is maintained by Cybers Thang (OdooSMES)

Contact information: `ducthangict.dhtn@gmail.com <ducthangict.dhtn@gmail.com>`_.
Skype: `live:ictlucky.dhtn <live:ictlucky.dhtn>`_.
Website: `https://odoosmes.com <https://odoosmes.com>`_.